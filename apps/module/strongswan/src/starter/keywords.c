/* C code produced by gperf version 3.0.4 */
/* Command-line: /usr/bin/gperf -m 10 -C -G -D -t  */
/* Computed positions: -k'2-3,6,$' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif


/*
 * Copyright (C) 2005 Andreas Steffen
 * Hochschule fuer Technik Rapperswil, Switzerland
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.  See <http://www.fsf.org/copyleft/gpl.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

#include <string.h>

#include "keywords.h"

struct kw_entry {
    char *name;
    kw_token_t token;
};

#define TOTAL_KEYWORDS 191
#define MIN_WORD_LENGTH 2
#define MAX_WORD_LENGTH 17
#define MIN_HASH_VALUE 7
#define MAX_HASH_VALUE 390
/* maximum key range = 384, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (str, len)
     register const char *str;
     register unsigned int len;
{
  static const unsigned short asso_values[] =
    {
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391,   8,
      214, 391,  44, 391,  32, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391,  92, 391,  41,   1, 136,
       57,   5,   2,  12, 131,   1, 391, 143,  71,  18,
       27, 111,  87,   2,  18,   1,  21, 145,  61, 391,
      391,  63,   3, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391, 391, 391, 391, 391,
      391, 391, 391, 391, 391, 391
    };
  register int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[5]];
      /*FALLTHROUGH*/
      case 5:
      case 4:
      case 3:
        hval += asso_values[(unsigned char)str[2]];
      /*FALLTHROUGH*/
      case 2:
        hval += asso_values[(unsigned char)str[1]];
        break;
    }
  return hval + asso_values[(unsigned char)str[len - 1]];
}

static const struct kw_entry wordlist[] =
  {
    {"pfs",               KW_PFS_DEPRECATED},
    {"lifetime",          KW_KEYLIFE},
    {"rightimei",         KW_RIGHTIMEI},
    {"rightsourceif",     KW_RIGHTSOURCEIF},
    {"left",              KW_LEFT},
    {"leftimei",          KW_LEFTIMEI},
    {"leftgroups",        KW_LEFTGROUPS},
    {"rightgroups",       KW_RIGHTGROUPS},
    {"right",             KW_RIGHT},
    {"aggressive",        KW_AGGRESSIVE},
    {"leftcert",          KW_LEFTCERT},
    {"leftdns",           KW_LEFTDNS},
    {"uniqueids",         KW_UNIQUEIDS},
    {"leftsendcert",      KW_LEFTSENDCERT},
    {"rightsubnet",       KW_RIGHTSUBNET},
    {"rightikeport",      KW_RIGHTIKEPORT},
    {"rightsendcert",     KW_RIGHTSENDCERT},
    {"rightintsubnet",    KW_RIGHTINTSUBNET},
    {"rightimeiformat",   KW_RIGHTIMEIFORMAT},
    {"rightikeportnatt",  KW_RIGHTIKEPORTNATT},
    {"rightidr_apn",      KW_RIGHTIDR_APN},
    {"lifepackets",       KW_LIFEPACKETS},
    {"certuribase",       KW_CERTURIBASE},
    {"rightsubnetwithin", KW_RIGHTSUBNET},
    {"leftprotoport",     KW_LEFTPROTOPORT},
    {"leftimeiformat",    KW_LEFTIMEIFORMAT},
    {"interfaces",        KW_SETUP_DEPRECATED},
    {"interface",         KW_INTERFACE},
    {"keep_alive",        KW_SETUP_DEPRECATED},
    {"retrans_tries",     KW_RETRANS_TRIES},
    {"leftintsubnet",     KW_LEFTINTSUBNET},
    {"reqid",             KW_REQID},
    {"retrans_base",      KW_RETRANS_BASE},
    {"rightforcetsi64",   KW_RIGHTFORCETSI64},
    {"server_nocert",     KW_SERVER_NOCERT},
    {"lifebytes",         KW_LIFEBYTES},
    {"rightid",           KW_RIGHTID},
    {"rightdns",          KW_RIGHTDNS},
    {"virtual_private",   KW_SETUP_DEPRECATED},
    {"leftsigkey",        KW_LEFTSIGKEY},
    {"keylife",           KW_KEYLIFE},
    {"leftrsasigkey",     KW_LEFTSIGKEY},
    {"rightsigkey",       KW_RIGHTSIGKEY},
    {"leftcertpolicy",    KW_LEFTCERTPOLICY},
    {"leftfirewall",      KW_LEFTFIREWALL},
    {"keyingtries",       KW_KEYINGTRIES},
    {"mark_in",           KW_MARK_IN},
    {"leftca",            KW_LEFTCA},
    {"crluri",            KW_CRLURI},
    {"marginbytes",       KW_MARGINBYTES},
    {"rightfirewall",     KW_RIGHTFIREWALL},
    {"marginpackets",     KW_MARGINPACKETS},
    {"margintime",        KW_REKEYMARGIN},
    {"leftidr_apn",       KW_LEFTIDR_APN},
    {"rightforcetsifull", KW_RIGHTFORCETSIFULL},
    {"fragmentation",     KW_FRAGMENTATION},
    {"crluri1",           KW_CRLURI},
    {"use_cfg_vip",       KW_USE_CFG_VIP},
    {"rightrsasigkey",    KW_RIGHTSIGKEY},
    {"leftnexthop",       KW_LEFT_DEPRECATED},
    {"rightpcscf",        KW_RIGHTPCSCF},
    {"rightsourceip",     KW_RIGHTSOURCEIP},
    {"certreq_critical",  KW_CERTREQ_CRITICAL},
    {"mediation",         KW_MEDIATION},
    {"leftid",            KW_LEFTID},
    {"mobike",	           KW_MOBIKE},
    {"rightallowany",     KW_RIGHTALLOWANY},
    {"leftupdown",        KW_LEFTUPDOWN},
    {"leftsourceif",      KW_LEFTSOURCEIF},
    {"lefthostaccess",    KW_LEFTHOSTACCESS},
    {"rightprotoport",    KW_RIGHTPROTOPORT},
    {"strictcrlpolicy",   KW_STRICTCRLPOLICY},
    {"rightnexthop",      KW_RIGHT_DEPRECATED},
    {"inactivity",        KW_INACTIVITY},
    {"compress",          KW_COMPRESS},
    {"forceencaps",       KW_FORCEENCAPS},
    {"device_identity",   KW_DEVICE_IDENTITY},
    {"hashandurl",        KW_HASHANDURL},
    {"klipsdebug",        KW_SETUP_DEPRECATED},
    {"ldapbase",          KW_CA_DEPRECATED},
    {"leftallowany",      KW_LEFTALLOWANY},
    {"leftpcscf",         KW_LEFTPCSCF},
    {"ike",               KW_IKE},
    {"mediated_by",       KW_MEDIATED_BY},
    {"keepalivedelay",    KW_KEEPALIVEDELAY},
    {"type",              KW_TYPE},
    {"righthostaccess",   KW_RIGHTHOSTACCESS},
    {"oostimeout",        KW_OOSTIMEOUT},
    {"rekeyfuzz",         KW_REKEYFUZZ},
    {"ocspuri",           KW_OCSPURI},
    {"nat_traversal",     KW_SETUP_DEPRECATED},
    {"rightcustcpimei",   KW_RIGHTCUSTCPIMEI},
    {"ikelifetime",       KW_IKELIFETIME},
    {"leftcustcpimei",    KW_LEFTCUSTCPIMEI},
    {"me_peerid",         KW_ME_PEERID},
    {"ocspuri1",          KW_OCSPURI},
    {"rightintnetmask",   KW_RIGHTINTNETMASK},
    {"is_emergency",      KW_IS_EMERGENCY},
    {"retrans_to",        KW_RETRANS_TO},
    {"installpolicy",     KW_INSTALLPOLICY},
    {"leftforcetsi64",    KW_LEFTFORCETSI64},
    {"hidetos",           KW_SETUP_DEPRECATED},
    {"esp",               KW_ESP},
    {"rightcert",         KW_RIGHTCERT},
    {"crlcheckinterval",  KW_SETUP_DEPRECATED},
    {"leftikeport",       KW_LEFTIKEPORT},
    {"leftsubnet",        KW_LEFTSUBNET},
    {"leftikeportnatt",   KW_LEFTIKEPORTNATT},
    {"also",              KW_ALSO},
    {"reauth_mbb",        KW_REAUTH_MBB},
    {"dumpdir",           KW_SETUP_DEPRECATED},
    {"leftintnetmask",    KW_LEFTINTNETMASK},
    {"detach_timeout",    KW_DETACH_TIMEOUT},
    {"dpdtimeout",        KW_DPDTIMEOUT},
    {"rightauth",         KW_RIGHTAUTH},
    {"leftsubnetwithin",  KW_LEFTSUBNET},
    {"rightupdown",       KW_RIGHTUPDOWN},
    {"rightca",           KW_RIGHTCA},
    {"rightcustcppcscf6", KW_RIGHTCUSTCPPCSCF6},
    {"mark_out",          KW_MARK_OUT},
    {"leftcustcppcscf6",  KW_LEFTCUSTCPPCSCF6},
    {"dpdaction",         KW_DPDACTION},
    {"fast_reauth",       KW_FAST_REAUTH},
    {"rekeymargin",       KW_REKEYMARGIN},
    {"leftforcetsifull",  KW_LEFTFORCETSIFULL},
    {"mark",              KW_MARK},
    {"pfsgroup",          KW_PFS_DEPRECATED},
    {"rightcustcppcscf4", KW_RIGHTCUSTCPPCSCF4},
    {"leftcustcppcscf4",  KW_LEFTCUSTCPPCSCF4},
    {"aaa_identity",      KW_AAA_IDENTITY},
    {"rekey",             KW_REKEY},
    {"leftsourceip",      KW_LEFTSOURCEIP},
    {"eap",               KW_CONN_DEPRECATED},
    {"keyexchange",       KW_KEYEXCHANGE},
    {"charondebug",       KW_CHARONDEBUG},
    {"overridemtu",       KW_SETUP_DEPRECATED},
    {"liveness_check",    KW_LIVENESS_CHECK},
    {"cacert",            KW_CACERT},
    {"rightcertpolicy",   KW_RIGHTCERTPOLICY},
    {"ocsp",              KW_OCSP},
    {"charonstart",       KW_SETUP_DEPRECATED},
    {"packetdefault",     KW_SETUP_DEPRECATED},
    {"no_initct",         KW_NO_INIT_CONTACT},
    {"leftcert2",         KW_LEFTCERT2},
    {"rightid2",          KW_RIGHTID2},
    {"wdrv_keepalive",    KW_WDRV_KEEPALIVE},
    {"ldaphost",          KW_CA_DEPRECATED},
    {"force_keepalive",   KW_SETUP_DEPRECATED},
    {"plutostderrlog",    KW_SETUP_DEPRECATED},
    {"encrkeydisplay",    KW_ENCRKEYDISPLAY},
    {"pcscf_restore",     KW_PCSCF_RESTORE},
    {"plutostart",        KW_SETUP_DEPRECATED},
    {"leftgroups2",       KW_LEFTGROUPS2},
    {"rightgroups2",      KW_RIGHTGROUPS2},
    {"eap_identity",      KW_EAP_IDENTITY},
    {"closeaction",       KW_CLOSEACTION},
    {"ah",                KW_AH},
    {"leftca2",           KW_LEFTCA2},
    {"tfc",               KW_TFC},
    {"rightauth2",        KW_RIGHTAUTH2},
    {"auto",              KW_AUTO},
    {"leftid2",           KW_LEFTID2},
    {"dpddelay",          KW_DPDDELAY},
    {"prepluto",          KW_SETUP_DEPRECATED},
    {"fragicmp",          KW_SETUP_DEPRECATED},
    {"leftauth",          KW_LEFTAUTH},
    {"plutodebug",        KW_SETUP_DEPRECATED},
    {"authby",            KW_AUTHBY},
    {"modeconfig",        KW_MODECONFIG},
    {"pkcs11initargs",    KW_PKCS11_DEPRECATED},
    {"postpluto",         KW_SETUP_DEPRECATED},
    {"pkcs11module",      KW_PKCS11_DEPRECATED},
    {"pkcs11keepstate",   KW_PKCS11_DEPRECATED},
    {"skipcheckcert",     KW_SKIPCHECKCERT},
    {"crluri2",           KW_CRLURI2},
    {"reauth",            KW_REAUTH},
    {"nocrsend",          KW_SETUP_DEPRECATED},
    {"xauth",             KW_XAUTH},
    {"cachecrls",         KW_CACHECRLS},
    {"dpd_noreply",       KW_DPD_NOREPLY},
    {"skipcheckid",       KW_SKIPCHECKID},
    {"xauth_identity",    KW_XAUTH_IDENTITY},
    {"pkcs11proxy",       KW_PKCS11_DEPRECATED},
    {"no_eaponly",        KW_NO_EAP_ONLY},
    {"rightca2",          KW_RIGHTCA2},
    {"rightcert2",        KW_RIGHTCERT2},
    {"leftauth2",         KW_LEFTAUTH2},
    {"wsharkfiledump",    KW_WSHARKFILEDUMP},
    {"ocspuri2",          KW_OCSPURI2},
    {"ikedscp",           KW_IKEDSCP,},
    {"addrchg_reauth",    KW_ADDRCHG_REAUTH}
  };

static const short lookup[] =
  {
     -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,  -1,  -1,
     -1,  -1,  -1,  -1,  -1,  -1,  -1,   1,  -1,  -1,
     -1,  -1,  -1,  -1,   2,  -1,  -1,  -1,  -1,   3,
     -1,  -1,   4,  -1,   5,  -1,   6,   7,  -1,   8,
      9,  10,  11,  12,  -1,  13,  14,  15,  16,  17,
     18,  19,  -1,  20,  -1,  -1,  21,  22,  23,  24,
     25,  26,  -1,  -1,  27,  -1,  28,  29,  30,  31,
     32,  -1,  -1,  -1,  33,  34,  35,  -1,  36,  37,
     38,  39,  40,  -1,  41,  -1,  -1,  -1,  42,  43,
     -1,  44,  45,  -1,  46,  47,  -1,  48,  49,  50,
     51,  52,  53,  54,  55,  56,  57,  -1,  58,  -1,
     59,  -1,  60,  -1,  61,  62,  -1,  -1,  -1,  63,
     -1,  -1,  -1,  -1,  -1,  -1,  -1,  64,  65,  -1,
     66,  67,  68,  69,  -1,  70,  -1,  -1,  71,  72,
     -1,  -1,  73,  74,  -1,  -1,  75,  -1,  -1,  76,
     77,  78,  79,  80,  81,  -1,  82,  83,  84,  85,
     86,  87,  88,  89,  90,  91,  92,  93,  94,  -1,
     -1,  95,  96,  97,  98,  99, 100, 101, 102, 103,
     -1, 104, 105, 106,  -1,  -1, 107, 108, 109, 110,
     -1, 111, 112, 113, 114, 115, 116, 117, 118, 119,
    120, 121, 122,  -1, 123, 124, 125,  -1,  -1, 126,
    127,  -1, 128,  -1, 129,  -1, 130, 131, 132,  -1,
    133,  -1, 134, 135, 136, 137,  -1, 138, 139,  -1,
     -1, 140, 141,  -1, 142, 143, 144, 145, 146,  -1,
     -1, 147,  -1, 148,  -1, 149,  -1, 150, 151,  -1,
    152, 153,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
    154, 155,  -1,  -1, 156,  -1,  -1,  -1,  -1, 157,
     -1,  -1,  -1,  -1,  -1,  -1,  -1, 158, 159,  -1,
     -1, 160,  -1,  -1,  -1, 161, 162, 163,  -1,  -1,
    164, 165,  -1,  -1,  -1, 166,  -1,  -1, 167,  -1,
     -1, 168, 169, 170, 171,  -1,  -1, 172,  -1, 173,
     -1, 174,  -1,  -1, 175,  -1,  -1, 176,  -1,  -1,
     -1,  -1, 177, 178,  -1,  -1,  -1,  -1,  -1, 179,
     -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
     -1,  -1,  -1, 180,  -1,  -1,  -1,  -1,  -1,  -1,
     -1,  -1,  -1,  -1,  -1, 181,  -1,  -1,  -1,  -1,
     -1, 182,  -1, 183,  -1,  -1,  -1,  -1,  -1,  -1,
     -1, 184,  -1, 185,  -1, 186, 187, 188, 189,  -1,
     -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
    190
  };

#ifdef __GNUC__
__inline
#if defined __GNUC_STDC_INLINE__ || defined __GNUC_GNU_INLINE__
__attribute__ ((__gnu_inline__))
#endif
#endif
const struct kw_entry *
in_word_set (str, len)
     register const char *str;
     register unsigned int len;
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register int index = lookup[key];

          if (index >= 0)
            {
              register const char *s = wordlist[index].name;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return &wordlist[index];
            }
        }
    }
  return 0;
}
